package steamapi

import (
    "net/url"
    "strconv"
    "strings"
)

type SteamID uint64

func (s SteamID) String() string {
    return strconv.FormatUint(uint64(s), 10)
}

func (s *SteamID) UnmarshalText(b []byte) error {
    id, err := strconv.ParseUint(string(b), 0, 64)
    if err != nil {
        return err
    }
    *s = SteamID(id)
    return nil
}

type CommunityVisibilityState int

const (
    Private CommunityVisibilityState = iota + 1
    FriendsOnly
    Public
)

type PersonaState int

func (s PersonaState) String() string {
    switch s {
    case Offline:
        return "Offline"
    case Online:
        return "Online"
    case Busy:
        return "Busy"
    case Away:
        return "Away"
    case Snooze:
        return "Snooze"
    case LookingToTrade:
        return "Looking to trade"
    case LookingToPlay:
        return "Looking to play"
    default:
        return "Unknown"
    }
}

const (
    Offline PersonaState = iota
    Online
    Busy
    Away
    Snooze
    LookingToTrade
    LookingToPlay
)

type PlayerSummary struct {
    SteamID         uint64                      `json:",string"`
    VisibilityState CommunityVisibilityState    `json:"communityvisibilitystate"`
    ProfileURL      string

    ProfileState    int
    PersonaName     string
    LastLogoff      int64
    PersonaState    PersonaState

    SmallAvatarURL  string                      `json:"avatar"`         // 32x32
    MediumAvatarURL string                      `json:"avatarmedium"`   // 64x64
    LargeAvatarURL  string                      `json:"avatarfull"`     // 184x184

    TimeCreated     int64                       `json:",omitempty"`
    RealName        string                      `json:",omitempty"`
    GameExtraInfo   string                      `json:",omitempty"`

    PrimaryClanID   uint64                      `json:",string,omitempty"`
}

func GetPlayerSummaries(ids []SteamID) ([]PlayerSummary, error) {
    s := method{
        Interface: "ISteamUser",
        Method:    "GetPlayerSummaries",
        Version:   2,
    }
    strIds := make([]string, len(ids))
    for _, id := range ids {
        strIds = append(strIds, id.String())
    }
    u := url.Values{}
    u.Add("steamids", strings.Join(strIds, ","))
    var resp struct{
        Response struct {
            Players []PlayerSummary
        }
    }
    err := s.Request(u, &resp)
    if err != nil {
        return nil, err
    }
    return resp.Response.Players, nil
}

type ResolveVanityURLResponse struct {
    Success     int
    SteamID     uint64  `json:",omitempty,string"`
    Message     string  `json:",omitempty"`
}

func ResolveVanityURL(vanityURL string) (*ResolveVanityURLResponse, error) {
    s := method{
        Interface: "ISteamUser",
        Method:    "ResolveVanityURL",
        Version:   1,
    }
    u := url.Values{}
    u.Add("vanityURL", vanityURL)
    var res ResolveVanityURLResponse
    err := s.Request(u, &res)
    if err != nil {
        return nil, err
    }
    if res.Success != 1 {
        err = Error(res.Message)
    }
    return &res, err
}
