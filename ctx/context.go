package ctx

import "context"

var c context.Context

func Context() context.Context {
    if c == nil {
        c = context.Background()
    }
    return c
}

func WithValue(key, val interface{}) context.Context {
    c = context.WithValue(Context(), key, val)
    return c
}

func Value(key interface{}) interface{} {
    return Context().Value(key)
}
