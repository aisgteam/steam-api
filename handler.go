package steamapi

import (
    "context"
    "net/http"
    "net/url"
    "regexp"

    "bitbucket.org/aisgteam/steam-api/ctx"
)

const (
    openidUrl = "https://steamcommunity.com/openid"
    openidMode = "checkid_setup"
    openidNs = "http://specs.openid.net/auth/2.0"
    openidNsSreg = "http://openid.net/extensions/sreg/1.1"
    openidIdentity = "http://specs.openid.net/auth/2.0/identifier_select"
    openidClaimedId = "http://specs.openid.net/auth/2.0/identifier_select"
    getParamName = "state"
    getParamValue = "loginsteam"
)

var reSteamID = regexp.MustCompile(`https:\/\/steamcommunity\.com\/openid\/id\/([0-9]{17,25})`)

func Use(h http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        if _, ok := ctx.Value(contextKey).(*Client); ok {
            r.ParseForm()
            state := r.FormValue(getParamName)
            if state != getParamValue {
                returnTo, err := url.ParseRequestURI(r.RequestURI)
                if err == nil {
                    returnTo.Scheme = "http"
                    returnTo.Host = r.Host
                    q := returnTo.Query()
                    q.Set(getParamName, getParamValue)
                    returnTo.RawQuery = q.Encode()
                    redirect(returnTo, w, r)
                }
            } else {
                identity := r.FormValue("openid.identity")
                if identity != "" {
                    steamId := SteamID(0)
                    match := reSteamID.FindStringSubmatch(identity)
                    if len(match) > 1 {
                        if err := steamId.UnmarshalText([]byte(match[1])); err == nil {
                            users, err := GetPlayerSummaries([]SteamID{steamId})
                            if err == nil {
                                r = r.WithContext(context.WithValue(r.Context(), "steamUser", users[0]))
                            }
                        }
                    }
                }
                h.ServeHTTP(w, r)
            }
        } else {
            w.WriteHeader(http.StatusInternalServerError)
        }
    })
}

func redirect(u *url.URL, w http.ResponseWriter, r *http.Request) {
    redirectTo, err := url.Parse(openidUrl)
    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
    } else {
        redirectTo.Path += "/login"
        q := redirectTo.Query()
        q.Set("openid.ns", openidNs)
        q.Set("openid.mode", openidMode)
        q.Set("openid.return_to", u.String())
        q.Set("openid.realm", u.Scheme + "://" + u.Host)
        q.Set("openid.ns.sreg", openidNsSreg)
        q.Set("openid.identity", openidIdentity)
        q.Set("openid.claimed_id", openidClaimedId)
        redirectTo.RawQuery = q.Encode()
        http.Redirect(w, r, redirectTo.String(), http.StatusSeeOther)
    }
}
