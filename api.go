package steamapi

import (
    "encoding/json"
    "fmt"
    "net/http"
    "net/url"
    "strconv"

    aiapic "bitbucket.org/aisgteam/ai-api-client"
    aistruct "bitbucket.org/aisgteam/ai-struct"
    "bitbucket.org/aisgteam/steam-api/ctx"
    "github.com/afex/hystrix-go/hystrix"
)

var a *Client

type Client struct {
    aiapic.HystrixClient
}

const (
    contextKey = "steamapi"
)

func New(key string) (*Client, error) {
    if key == "" {
        return nil, NoKey
    }
    a = &Client{}
    a.Name = "Golang Steam Api Client"
    a.Version = aistruct.Version{1, 0, 0, 0}
    a.UserAgent = a.FullName()
    err := a.New("http://api.steampowered.com")
    if err != nil {
        return nil, err
    }
    a.Auth = &Auth{Key:key}

    hystrix.ConfigureCommand(a.Name, hystrix.CommandConfig{
        Timeout: 50000,
        MaxConcurrentRequests: 300,
        RequestVolumeThreshold: 10,
        SleepWindow: 1000,
        ErrorPercentThreshold: 10,
    })

    ctx.WithValue(contextKey, a)

    return a, nil
}

type Auth struct {
    Key     string
}

func (a *Auth) Use(r *http.Request) {
    q := r.URL.Query()
    q.Set("key", a.Key)
    r.URL.RawQuery = q.Encode()
}

type method struct {
    Interface       string
    Method          string
    Version         int
}

func (s method) String() string {
    return fmt.Sprintf("/%v/%v/v%v", s.Interface, s.Method, strconv.Itoa(s.Version))
}

func (s method) Request(data url.Values, i interface{}) error {
    v := ctx.Value(contextKey)
    if v == nil {
        return NoClient
    }
    api := v.(*Client)
    u := api.PrepareUri(s.String())
    if data != nil {
        u.RawQuery = data.Encode()
    }
    req, err := api.Prepare(http.MethodGet, u, nil)
    if err != nil {
        return err
    }
    res, err := api.Exec(req)
    if err != nil {
        return err
    }
    defer res.Body.Close()

    //bodyBytes, _ := ioutil.ReadAll(res.Body)
    //bodyString := string(bodyBytes)
    //fmt.Printf("\n\n%s\n\n", bodyString)
    //
    //res.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

    if res.StatusCode != 200 {
        return Error(fmt.Sprintf("%s Status code %d", s, res.StatusCode))
    }

    d := json.NewDecoder(res.Body)
    return d.Decode(&i)
}
