package steamapi

import "fmt"

type Error string

func (e Error) Error() string {
    return fmt.Sprintf("SteamApi: %s", string(e))
}

const (
    NoKey Error = "No Key"
    NoClient Error = "Not initialize client"
)
