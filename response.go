package steamapi

type ResponseFormat string

const (
    FormatJSON ResponseFormat = "json"
    FormatXML = "xml"
    FormatVDF = "vdf"
)

